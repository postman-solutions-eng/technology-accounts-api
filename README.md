# Technology Accounts API
A repository with a corresponding Postman workspace for demonstrating best practices for integrating with NOVA Cybersecurity APIs.

**This repository has a corresponding Postman workspace that is part of a set of demo workspaces the Postman Solutions Engineering team put together to show how different industries can utilize workspaces to suit their needs, and does not represent a real company. You can read more about the process of creating this workspace in the companion [blog post](https://blog.postman.com/postman-enterprise-features-automotive-technology-industries/)**  

![NOVA workspace](https://user-images.githubusercontent.com/20145532/129622947-92777f72-92da-4145-ba33-27dafad438d7.png)

## Welcome!
We have put together this workspace as a guide to showcase best practices around integrating NOVA Security API alongside our or third party apps. This is a working document put together by the integrations team, and we're always looking for ways to improve, so please let us know if there is something you think would be a good addition here. You can contact us by [leaving a comment](https://learning.postman.com/docs/collaborating-in-postman/commenting-on-collections/) on a collection, or by emailing integrations@novasecurity.com.

### Getting started
**The NOVA Security User Management API**:  
This is the official NOVA Security API documentation subset for integration partners. It covers admin tasks like getting registered user reports, network reports, found risks, and other misc. tasks available to admin users. 
 - **OpenAPI**: On the left side of the Postman screen, you'll see an `APIs` tab on the vertical toolbar. Clicking this, then navigating to the `NOVA Security API` will take you to the OpenAPI spec for the partner API. Here, you can see the OpenAPI YAML and navigate to previous versions. You will also be able to see the collections that have been generated from this spec.
 - **Monitor**: Under the Monitors tab on the same vertical toolbar, we have set up a monitor that runs the collection once a day and displays the results of the tests. 
 - **Mock Server**: As this API is liable to expose sensitive information, we have set up a mock server to return example responses. You can still send the requests, just make sure the `baseUrl` varialbe is filled in with the monitor url to receive a response.
 - **Visualizations**: To make data easier to spot-check, we have implemented a visualization on the `GET All API Users` request that lets you search and filter users by their name, id, or last login. 

 <img alt="user table visualization" src="https://user-images.githubusercontent.com/20145532/129748924-1adbfacb-c281-4c2d-8c0d-0eef38d0cbf9.gif" width="500">

 **OAuth Collection**:
This is a fork of Blizzard's OAuth collection from the Public API Network that we use to integrate with our partners. When interacting with the NOVA Client Services, you may need to pull in requests from this collection to authenticate and continue on with your workflow. For questions and comments around this workflow, you may either leave a comment on the original source collection, or reach out to the Security team. 

 **Security Audit**:
 This collection is a workflow for testing the security audit process. Each request has tests that can be run individually, or via the Collection Runner.
 - This collection is also available to be run with [Newman](https://learning.postman.com/docs/running-collections/using-newman-cli/command-line-integration-with-newman/#:~:text=Newman%20is%20a%20command%2Dline,directly%20from%20the%20command%20line.&text=Newman%20maintains%20feature%20parity%20with,the%20collection%20runner%20in%20Postman.) if you would like to use it in a CI/CD pipeline. It is recommended that you create a data file to include in your run and iterate through the different network values. Contact Taylor on the QA team for more info on how we have done this in the past.